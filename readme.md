## static website https://nathan.pizza

#### Stack
* [metalsmith](http://www.metalsmith.io/)
* [webpack](https://webpack.github.io/)
* [libsass](https://github.com/sass/node-sass)
* [swig](https://paularmstrong.github.io/swig/)
* [s3](https://aws.amazon.com/s3/)

#### Setup
0. `npm i`
0. `npm run dev`
