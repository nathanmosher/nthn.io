
var Metalsmith = require('metalsmith');
var collections = require('metalsmith-collections');
var layouts = require('metalsmith-layouts');
var mswebpack = require('metalsmith-webpack');
var assets = require('metalsmith-assets');
var inPlace = require('metalsmith-in-place');
var sass = require('metalsmith-sass');
var ignore = require('metalsmith-ignore');
var filenames = require('metalsmith-filenames');
var permalinks = require('metalsmith-permalinks');
var beautify = require('metalsmith-beautify');
var uglify = require('metalsmith-uglify');
var autoprefixer = require('metalsmith-autoprefixer');

var path = require('path');
var webpack = require('webpack');

var args = require('minimist')(process.argv.slice(2));
// console.dir(args.dev);

var metalsmith = Metalsmith(__dirname + '/../')
    .destination('dist')
    // .use(assets({
    //   source: './static', // relative to the working directory
    //   destination: './static' // relative to the build directory
    // }))
    // .use(collections({
    //   "articles": {
    //     "pattern": "*.md",
    //     "sortBy": "date",
    //     "reverse": true
    //   }
    // }))
    .use(assets({
      source: './src/static', // relative to the working directory
      destination: './static' // relative to the build directory
    }))
    .use(filenames())
    .use(sass({
      outputStyle: (args.dev) ? 'compact' : 'compressed',
      sourceMapEmbed: (args.dev) ? true : false,
      includePaths: [
        'node_modules',
        'bower_components',
        require('node-reset-scss').includePath
      ]
    }))
    .use(autoprefixer())
    .use(inPlace({
      engine: 'swig',
      cache: false,
      partials: 'partials',
      pattern: '*.html'
    }))
    .use(layouts({
      engine: 'swig'
    }))
    .use(mswebpack({
      context: path.resolve(__dirname, '../src/js/'),
      entry: './main.js',
      devtool: (args.dev) ? 'inline-source-map' : false,
      output: {
        path: '/js',
        filename: 'main.js'
      },
      plugins: [
        new webpack.optimize.UglifyJsPlugin({minimize: true})
      ]
    }))
    .use(ignore([
      'js/components/**/*',
      'common/**/*',
      'layouts/**/*',
    ]))
    .use(permalinks({

    }))
    .use(beautify({
      css: false,
      html: true,
      js: false,
      indent_size: 2,
      indent_char: ' '
    }));

module.exports = exports = metalsmith;
