var bs = require('metalsmith-browser-sync');

var ms = require('./metalsmith');

ms.use(bs({
    server : 'dist',
    files  : ['src/**/*'],
    notify: false
  })
);

require('./build');
