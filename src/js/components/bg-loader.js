var $ = require('jquery');
var _ = require('lodash');

var elements = $(document.querySelectorAll('[bg-loader]'));

$.each(elements, function (i, element) {
  var $el = $(element);

  var bg = $el.css('background-image')
    .replace(/^url\(/, '')
    .replace(/\)$/, '')
    .replace(/"/g, '');

  $('<img/>').attr('src', bg).load(function() {
     $(this).remove(); // prevent memory leaks as @benweet suggested
     $el.closest('.bg-loading').removeClass('bg-loading');
  });
});
