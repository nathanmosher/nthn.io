var $ = require('jquery');

var element = $(document.querySelector('[typer]'));

var likes = [
  'strongly typed supersets of JavaScript.',
  'riding <a href="https://www.instagram.com/p/rxzR8BJYAI/">this bad boy</a> around town.',
  'listening to synthesizer-y music.',
  'eating ungodly amounts of Nashville <a href="//en.wikipedia.org/wiki/Hot_chicken">hot chicken</a>.',
  'to await my asyncs.',
  'doing front side <a href="https://www.instagram.com/p/BH_BsyHjLNK/">blunt slides</a> on marble ledges.',
  'hanging out with <a href="https://www.instagram.com/p/BhNViLWDmQ2/">Saint Bernard puppies</a>.',
  'cruisin\' down Valencia on the <a href="https://www.instagram.com/p/xZ_SRmpYOy/">zip zinger</a>.',
  'eating chicken tikka masala from <a href="https://pakwanrestaurant.com/">Pakwan Restaurant</a>.',
  'wearing the same <a href="https://www.instagram.com/p/BwyF9gKBhmR/">pair of shoes</a> for <a href="https://www.instagram.com/p/qSu8K1JYKN/">5 years</a>.',
];

var TYPE_SPEED = 70;

likes.push(element.text());

var getRandomSpeed = function () {
  // var min = 20;
  // var max = 130;
  // var rand = Math.floor(Math.random() * (max - min + 1)) + min;
  var rand = 20;
  return rand;
};

var teardown = function (element, i, cb) {
  var currentText = likes[i];
  var j = 0;
  var decrementor;

  element.html(createHtml(currentText));
  var interval = setTimeout(decrementor = function () {
    var str = element.text()
    var next = currentText.substring(0, j);
    if (j <= currentText.length) {

      // element.text(next);
      var span = element[0].querySelector('.char-'+j);
      var $span = $(span);
      window.requestAnimationFrame(function () {
        $span.addClass('hidden');
        $('.last-visible').removeClass('last-visible');
        $span.prev().addClass('last-visible');
      });

      j++;
      setTimeout(decrementor, getRandomSpeed())
    } else {
      if(cb) cb(i);
    }

  }, getRandomSpeed());
};

var buildup = function (element, i, cb) {
  var currentText = likes[i];
  var j = 0;
  var incrementor;
  element.html(createHtml(currentText, true));
  var interval = setTimeout(incrementor = function () {
    var str = element.text()
    var next = currentText.substring(0, j);
    if (j <= currentText.length) {

      var span = element[0].querySelector('.char-'+j);
      var $span = $(span);
      window.requestAnimationFrame(function () {
        $span.removeClass('hidden');
        $('.last-visible').removeClass('last-visible');
        $span.addClass('last-visible');
      });
      j++;
      setTimeout(incrementor, getRandomSpeed())

    } else {
      if(cb) cb(i);
    }

  }, getRandomSpeed());
};

var createHtml = function (wholeStr, hidden) {

  var html = "";
  var j = 0;

  for(var i = 0; i < wholeStr.length; i++) {

    var letter = wholeStr.charAt(i);
    var whatsLeft = wholeStr.slice(i,wholeStr.length);

    var tagBeginRegex = /<(a|s)[^>]*>/;
    var tagBeginIndex = whatsLeft.search(tagBeginRegex);

    var tagEndRegex = /<\/(a|s)>/
    var tagEndIndex = whatsLeft.search(tagEndRegex)

    // Crazy ass logic to keep from wrapping anchor tags with spans.
    if(tagBeginIndex == 0){
      var openTag = whatsLeft.match(tagBeginRegex);
      html += openTag[0];
      i = i + openTag[0].length-1;
    } else if (tagEndIndex == 0) {
      var closeTag = whatsLeft.match(tagEndRegex);
      html += closeTag[0];
      i = i + closeTag[0].length-1;
    } else {
      html += '<span class="char-'+(++j);
      if (hidden) html += ' hidden';
      html += '">'+letter+'</span>';
    }
  };
  return html;
};

var looper;
(looper = function(i) {
  setTimeout((function() {

    if (i) {

      teardown(element, i, function () {
        setTimeout(function() {
          buildup(element, i, looper);
        }, 300);
      });

    } else {
      teardown(element, 0, function () {
        buildup(element, likes.length-1, looper);
      })
    }
    i--;

  }), 5000);
})(likes.length-1);
