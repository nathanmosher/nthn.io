var $ = require('jquery');
var _ = require('lodash');

var elements = $(document.querySelectorAll('[bg-fix]'));
var $window = $(window);

$.each(elements, function (i, element) {
  var $element = $(element);
  $window.on('scroll', function () {

    window.requestAnimationFrame(function () {
      var pos = 'translate3d(0px, '+ $window.scrollTop()*0.25 +'px, 0)';
      $element.css({transform: pos});
    });
  });
});
